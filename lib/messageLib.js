import fs from 'fs';

// Set Data path
const dataLocation = `${process.cwd()}/json/messages.json`;

// Retrieve messages middleware
const getMessageList = (req, res) => {
  // read the file
  fs.readFile(dataLocation, 'utf8', (err, data) => {
    if (err || !data) {
      console.log(`Error reading file from disk: ${err}`);
      return res.json([]);
    }
    // parse JSON to object and return
    const messages = JSON.parse(data);
    res.json(messages);
  });
};

// Save new message
const postMessage = (req, res) => {
  if (!Object.prototype.hasOwnProperty.call(req.body, 'text')) {
    res.statusCode = 400;
    return res.json({ error: 'Invalid message' });
  }

  // read the file
  fs.readFile(dataLocation, 'utf8', (err, data) => {
    if (err || !data) {
      console.log(`Error reading file from disk: ${err}`);
      return res.json([]);
    }

    // parse JSON string to JSON object
    const messages = JSON.parse(data);

    // add a new record
    messages.push({
      text: req.body.text,
      date: new Date().toLocaleString()
    });

    // write new data back to the file
    fs.writeFile(dataLocation, JSON.stringify(messages, null, 2), writeErr => {
      if (writeErr) {
        console.log(`Error writing file to disk: ${writeErr}`);
      }
      getMessageList(req, res);
    });
  });
};

export default {
  getMessageList,
  postMessage,
};

# README #

# Comparison Tech Coding Exercise

Brief: Please write a production ready Node.js application that works like a simple message board. This app should expose a rest interface that allows an anonymous user to submit messages and to retrieve a list of the submitted messages. Please follow sound engineering practices and due to the limited time available, please document any trade-offs that you had to make whilst building this app.

## Installing application

Install the project with `npm i`.

The application will run on port `3001` by default.

## Starting application

Start the project with `npm start`.

The application will run on port `3001` by default.

### Trade-Offs due to time limitation ###

Due to time limitations in development there were a number of trade-offs required. This app should be considered to be MVP.

- **Add transpiler** - As the front end JS code utilises ES6 & ES7, a transpiler should be used before deployment for backwards compatibility.

- **Data Persistance** - For speed, the application uses a physical JSON file structure for data persistance. It is intended for use by one user on one instance.

- **Error Handling** - Whilst the application is designed not to crash in the event of missing data storage requirements, there is nothing currently in place to notify the user of such events.

- **Add Tests** - Unit testing should be added to the application to ensure stability in future development cycles.

- **UX** - Current user experience is very basic in line with the task. Functionality improvemnets could include editing/removal of posts and layout improvements.

- **Reduce Stye overheads** Application utilises CSS from bootstrap - currently bloated beyond requirement.

- **Logging** User interaction should be logged. Public IP addresses of contributors should be recorded in case of legal breaches/ abuse etc.

- **Typescript** With more time, future development cycles could have benefited from the use of TypeScript.
import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import messageLib from './lib/messageLib.js';

const app = express();
const PORT = process.env.PORT || 3001;
const __dirname = process.cwd(); // const __dirname = path.resolve();

// Define Middleware
app
  .use(helmet())
  .use(compression())
  .use(bodyParser.json({ inflate: false })) // .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: true })) /// ?????
  .use(express.static(`${__dirname}/public`));

// Routes
app.get('/messages', messageLib.getMessageList);
app.post('/messages', messageLib.postMessage);

// Start App
app.listen(PORT, () => console.log(`[server]: Server is running at http://localhost:${PORT}`));

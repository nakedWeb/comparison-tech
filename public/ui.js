/* global fetch, document */

const addForm = document.querySelector('.add');
const messageList = document.querySelector('#message_list');
const apiUrl = '/messages';

const getMessages = async () => {
  const response = await fetch(apiUrl);

  if (response.status !== 200) {
    throw new Error('cannot fetch the data');
  }

  const data = await response.json();
  return data;
};

const postMessage = async (message) => {
  const data = {
    text: message
  };

  const config = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  };

  const response = await fetch(apiUrl, config);

  if (response.status !== 200) {
    throw new Error('cannot post the data');
  }

  const messages = await response.json();
  return messages;
};

const generateTemplate = message => {
  const newLi = document.createElement('li');
  newLi.innerHTML = `${message.text} <i>${message.date}</i>`; // textContent

  newLi.classList.add(
    'list-group-item',
    'd-flex',
    'justify-content-between',
    'align-items-center'
  );

  messageList.append(newLi);
};

const submitForm = e => {
  e.preventDefault();
  const newMessage = addForm.post_message.value.trim();

  if (newMessage.length) {
    postMessage(newMessage)
      .then((data) => {
        // Array.from(messageList.children).forEach(li => {
        //   console.log(li);
        //   li.remove();
        // });
        messageList.innerHTML = '';
        data.forEach(message => generateTemplate(message));
        addForm.reset();
      })
      .catch(err => console.log('rejected:', err.message));
  }
};

const initApp = () => {
  addForm.addEventListener('submit', submitForm);

  getMessages()
    .then(data => data.forEach(message => generateTemplate(message)))
    .catch(err => console.log('rejected:', err.message));
};

document.addEventListener('DOMContentLoaded', () => initApp());
